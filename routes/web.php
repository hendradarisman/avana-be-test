<?php

use Illuminate\Support\Facades\Route;
// use Test1\Parenthesized;
use Avana\Backendtest\Parenthesized;
use Avana\Backendtest\ValidationExcel;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route for Test 1 Check Parent
Route::get('/', function () {
    return view('avanaTest1');
});
Route::post('check-parent', 'CheckParentController@checkParent')->name('checkParent');
// End Route for Test 1
// Route for Test 2
Route::get('upload-excel', 'AvanaExcelController@upload')->name('upload');
// End Route for Test2


Route::get('/parent', function() {
    $coba = new Parenthesized();
    return $coba->output();
});


Route::get('/excel', function() {
    
    $path = Storage::path("Type_A");
    $coba = new ValidationExcel($path);
    // \Avana\Backendtest\Parenthesized::output();
     return $coba->output();
});