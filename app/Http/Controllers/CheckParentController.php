<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\CheckParentService;

class CheckParentController extends Controller
{
    protected $checkParentService;

    public function __construct(CheckParentService $checkParentService)
    {
        $this->checkParentService = $checkParentService;
    }
    
    public function checkParent(Request $request) {

        return $this->checkParentService->findIndexParent($request->input, $request->indexParent);
    }
}
