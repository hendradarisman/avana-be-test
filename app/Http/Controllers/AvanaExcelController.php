<?php
namespace App\Http\Controllers;

use App\Services\CheckExcelService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AvanaExcelController extends Controller
{
    protected $checkExcelService;

    public function __construct(CheckExcelService $checkExcelService)
    {
        $this->checkExcelService = $checkExcelService;
    }

    public function upload(Request $request){
         $this->checkExcelService->__construct('Type_A.xlsx');
         $this->checkExcelService->output();
         
         $this->checkExcelService->__construct('Type_B.xlsx');
         $this->checkExcelService->output();

    } 

    public function download(Request $request)
    {
        return $this->checkExcelService->download($request);
    }

}