<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Base class for repository
 *
 * @author Iqbal Hikmat Prahara <iqbal@logique.co.id>
 */
class BaseRepository implements RepositoryInterface
{
    /**
     * @var Model
     */
     protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
 
    /**
     * @param array $attributes
     *
     * @return Model
     */
    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    /**
     * @param array $values
     * 
     * @return bool
     */
    public function createMany(array $values)
    {
        return $this->model->insert($values);
    }

    /**
     * @return object Model
     */
    public function all(): Collection
    {
        return $this->model->all();
    }
 
    /**
     * @param $id 
     * 
     * @return Model
     */
    public function find($id): ?Model
    {
        return $this->model->find($id);
    }

    /**
     * @param array $attributes
     * 
     * @return Collection
     */
    public function findWhere(array $attributes): Collection
    {
        return $this->model->where($attributes)->get();
    }

    /**
     * @param $id
     * @param array $attributes
     * 
     * @return bool
     */
    public function update($id, array $attributes)
    {
        return $this->model->find($id)->update($attributes);
    }

    /**
     * @param $id
     * 
     * @return null
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }
}