<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Interface Repository Interface
 *
 * @author Iqbal Hikmat Prahara <iqbal@logique.co.id>
 */
interface RepositoryInterface
{
    /**
     * @param array $attributes
     * 
     * @return Model
     */
    public function create(array $attributes): Model;

    /**
     * @return Model
     */
    public function all(): Collection;

    /**
     * @param $id
     * 
     * @return Model
     */
    public function find($id): ?Model;

    /**
     * @param array $attributes
     * 
     * @return Collection
     */
    public function findWhere(array $attributes): Collection;

    /**
     * @param $id
     * @param array $attributes
     * 
     * @return Model
     */
    public function update($id, array $attributes);

    /**
     * @param $id
     * 
     * @return null
     */
    public function delete($id);
}
