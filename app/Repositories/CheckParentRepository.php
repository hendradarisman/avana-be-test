<?php

namespace App\Repositories;

use App\User;
use App\Repositories\BaseRepository;

class CheckParentRepository extends BaseRepository
{
   
    public function __construct(User $model)
    {
        parent::__construct($model);
    }
}