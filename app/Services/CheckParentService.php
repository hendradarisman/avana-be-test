<?php
 
namespace App\Services;

use App\Repositories\CheckParentRepository;

class CheckParentService
{
    protected $checkParentRepository;

    public function __construct(CheckParentRepository $checkParentRepository)
    {
        $this->checkParentRepository = $checkParentRepository;
    }

    public function findIndexParent($input, $indexParent)
    {
       
        if ($input[$indexParent] !== "(") {
            $error = [
                    "code" => 0,
                    "message" => "Index of Parent Failed"
            ];
            return response()->json($error, 422);
        }
        //Start Loop detail count base in entry
        $i = 1;
        while ($i > 0) {
            $indexParent += 1;
            $char = $input[$indexParent];
            if ($char === '(') {
                $i++;
            } else if ($char === ')') {
                $i--;
            }
        }
        //End Loop

        //sending return to controller
        return $indexParent;
    }
}