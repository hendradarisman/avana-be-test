<?php
namespace App\Services;


use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Storage;

class CheckExcelService
{

    const EXCEL_DATA_TYPE_ADMIN  = 'admin';
    const EXCEL_DATA_TYPE_MEMBER = 'member';


    public function __construct($filename= "Type_B.xlsx"){

        $this->objData = $this->generateExcel($filename);
        $this->sheet = $this->objData->getSheet(0); 
        $this->sheetData = $this->objData->getActiveSheet()->toArray(null,true,true,true); 
        $this->countRow = $this->sheet->getHighestRow(); 
        $this->checkOutput();
    }

    public function generateExcel($filename){
        $dataFile = Storage::path($filename);
        $dataType = IOFactory::identify($dataFile);
        $readData = IOFactory::createReader($dataType);
        $readData->setReadDataOnly(true);
        return $readData->load($dataFile);
    }

    public function checkOutput()            
    {
        for($i=2;$i<=$this->countRow;$i++){
            $dataOutput[$i] = '';
            foreach ($this->sheetData[$i] as $key => $value) {
                // dd((strpos($this->sheetData[1][$key], '#')));
                // exit;
                if (strpos($this->sheetData[1][$key], '*') !== false) {
                    if($value==null){
                        if($dataOutput[$i]==""){
                            $dataOutput[$i] .= 'Missing Value in '.str_replace("*","",$this->sheetData[1][$key]);
                        }
                        else{
                            $dataOutput[$i] .= ', Missing Value in '.str_replace("*","",$this->sheetData[1][$key]);
                        }
                    }
                        
                }
             
                else if(strpos($this->sheetData[1][$key], '#') !== false) {
                    if(preg_match('/\s/',$value)){
                        if($dataOutput[$i]==""){
                            $dataOutput[$i] .= str_replace("#","",$this->sheetData[1][$key]).' should not contain any space';
                        }
                        else{
                            $dataOutput[$i] .= ', '.str_replace("#","",$this->sheetData[1][$key]).' should not contain any space';
                        }
                            
                    }
                }    
            }
        }
        $this->dataOutput = $dataOutput;
    
    }

    public function output()
    {
        echo '<table>
            <tr>
                <th>Row</th>
                <th>Error</th>
            </tr>';

            foreach ($this->dataOutput as $key => $value) {
                if($value!=""){
                     echo '
                    <tr>
                        <td>'.$key.'</td>
                        <td>'.$value.'</td>
                    </tr>';
                }
            }
        echo '
        </table><br><br>';
    }

    // public function toExcel(string $fileName, array $title, array $data)
    // {
    //     try {
    //         Excel::create($fileName, function ($excel) use ($data, $title) {
    //             $excel->sheet('sheet', function ($sheet) use ($data, $title) {
    //                 try {
    //                     $sheet->fromArray($data, null, 'A1', true, false);
    //                 } catch (\Exception $e) {
    //                     return $this->failed(trans('api.failed.excel_export'), 400);
    //                 }
    //                 $sheet->prependRow($title);
    //             });
    //         })->store('xls', public_path('uploads/excel/exports'));

    //         $data =  [
    //             'download_url' => route('excel.download', ['file'=> public_path('/uploads/excel/exports/'.$fileName.'.xls')])
    //         ];
    //         return $this->success($data);
    //     } catch (\Exception $e) {
    //         return $this->failed(trans('api.failed.excel_export'), 400);
    //     }
    // }

}
