<?php
namespace Avana\Backendtest;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ValidationExcel
{

    protected $path;
    protected $valid = false;
    protected $file;
    protected $typeFile;


    public function __construct(string $filename){
        $this->file = $filename;
        $this->objData = $this->generateExcel($this->file);
        $this->sheet = $this->objData->getSheet(0); 
        $this->sheetData = $this->objData->getActiveSheet()->toArray(null,true,true,true); 
        $this->countRow = $this->sheet->getHighestRow(); 
        $this->checkOutput();
    }

    public function generateExcel(){
        //check extension
        $this->extFile();
        if($this->validExtension() === true) {
        // $dataFile = Storage::path($this->file);
        $dataType = IOFactory::identify($this->file);
        $readData = IOFactory::createReader($dataType);
        $readData->setReadDataOnly(true);
        return $readData->load($this->file);
        }
    }

    public function extFile()
    {
        $extension = ['xls', 'xlsx','csv'];

        foreach($extension as $extFile) {
            if (file_exists($this->file.".".$extFile)) {
             
                $this->valid         = true;
                $this->typeFile      = ucfirst($extFile);
                $this->file          = $this->file.".".$extFile;
                break;
            }
        }
    }

    public function validExtension()
    {
        return $this->valid;
    }

    public function checkOutput()            
    {
        if($this->valid === false) {
            echo "File Format extension not supported";
        }

        for($i=2;$i<=$this->countRow;$i++){
            $dataOutput[$i] = '';
            foreach ($this->sheetData[$i] as $key => $value) {
                // dd((strpos($this->sheetData[1][$key], '#')));
                // exit;
                if (strpos($this->sheetData[1][$key], '*') !== false) {
                    if($value==null){
                        if($dataOutput[$i]==""){
                            $dataOutput[$i] .= 'Missing Value in '.str_replace("*","",$this->sheetData[1][$key]);
                        }
                        else{
                            $dataOutput[$i] .= ', Missing Value in '.str_replace("*","",$this->sheetData[1][$key]);
                        }
                    }
                        
                }
             
                else if(strpos($this->sheetData[1][$key], '#') !== false) {
                    if(preg_match('/\s/',$value)){
                        if($dataOutput[$i]==""){
                            $dataOutput[$i] .= str_replace("#","",$this->sheetData[1][$key]).' should not contain any space';
                        }
                        else{
                            $dataOutput[$i] .= ', '.str_replace("#","",$this->sheetData[1][$key]).' should not contain any space';
                        }
                            
                    }
                }
                
            }
        }
       
       $this->dataOutput = $dataOutput;
    
    }

    public function output()
    {
        echo '<table>
            <tr>
                <th>Row</th>
                <th>Error</th>
            </tr>';
          
            foreach ($this->dataOutput as $key => $value) {
                if($value!=""){
                     echo '
                    <tr>
                        <td>'.$key.'</td>
                        <td>'.$value.'</td>
                    </tr>';
                }
            }
        echo '
        </table><br><br>';
    }
    

}
