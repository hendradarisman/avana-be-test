<?php

namespace Avana\Backendtest;

Class Parenthesized {

public function __construct(){
//    $this->input = "a (b c (d e (f) g) h) i (j k)";
//    $this->indexParent = 2;

}

  public function findIndexParent($input, $indexParent)
    {
       
        if ($input[$indexParent] !== "(") {
            $error = [
                    "code" => 0,
                    "message" => "Index of Parent Failed"
            ];
            return response()->json($error, 422);
        }
        //Start Loop detail count base in entry
        $i = 1;
        while ($i > 0) {
            $indexParent += 1;
            $char = $input[$indexParent];
            if ($char === '(') {
                $i++;
            } else if ($char === ')') {
                $i--;
            }
        }
        //End Loop

        //sending return to controller
        return $indexParent;
   
    
       
    }
    public function output()
    {
        $input = "a (b c (d e (f) g) h) i (j k)";
        $indexParent = 2;
        return $this->findIndexParent($input, $indexParent);
    }
    
    
  
}