<?php
require_once __DIR__ .'/vendor/autoload.php';

use Avana\Backendtest\Parenthesized;
use Avana\Backendtest\ValidationExcel;

    $validation = new ValidationExcel('Files/Type_A.xlsx');
    $validation->output();

    $validation = new ValidationExcel('Files/Type_B.xlsx');
    $validation->output();

    $parenthesis = new Parenthesized();
    $parenthesis->ouput();

