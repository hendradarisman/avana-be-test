# Testing Backend With Avana Indonesia

2 Test in one Application, For testing no 1 in routes ``/`` or http://test.local , for testing no 2 in routes ``/upload-excel`` getting default excel file in app storage

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
- PHP >= 7.2 
- Composer
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

### Installing
First installation
- Clone branch
- Pull from branch ```master``` 
- Create branch from ```master``` example ```git checkout -b nama-branch```

Second Installation
- Run ```composer install``` or ```composer update```
- Copy file `.env.example` and change name to `.env`
- Run `php artisan key:generate`

Third Installation
- Run ```php artisan migrate```
- Run `php artisan serve`

## Branch naming rules

Default branch Production ```master``` and Development ```develop``` 
- Create new feature in application please use format `feat/feature-name` Example : `feat/newreport`
- Create hotfix issue in application please use format `hotfix/hotfix-name` Example : `hotfix/issue-report`


## Versioning


### And coding style 

Please read and implement we use standard code PSR-4 [PHP PSR](https://www.php-fig.org/psr/)

## Deployment

Request access to Devops for testing on dev server

## Authors

 **Hendra Darisman** [Email](hendradarisman34@gmail.com)